.SUFFIXES:
.PHONY: default all clean mrproper indent run doc zip

include Makefile_config.mak

CC_FILES	:= $(shell find src -type f -iname '*.cc' ! -iname 'lisp.*.cc')
HH_FILES	:= $(shell find src  -type f -iname '*.hh' ! -iname 'lisp.*.hh')
O_FILES	:= $(CC_FILES:%.cc=%.o)

SOURCE_FLEX_FILES	:= src/User/Reader/lisp.lex
SOURCE_BISON_FILES	:= src/User/Reader/lisp.ypp

CC_FLEX_FILES	:= $(SOURCE_FLEX_FILES:%.lex=%.yy.cc)
O_FLEX_FILES	:= $(CC_FLEX_FILES:%.cc=%.o) 

CC_BISON_FILES	:= $(SOURCE_BISON_FILES:%.ypp=%.tab.cc)
O_BISON_FILES	:= $(CC_BISON_FILES:%.cc=%.o) 
HH_BISON_FILES	:= $(SOURCE_BISON_FILES:%.ypp=%.tab.hh)

SOURCE_READER_FILES	:= $(SOURCE_FLEX_FILES) $(SOURCE_BISON_FILES)
O_READER_FILES	:= $(O_FLEX_FILES) $(O_BISON_FILES)

HELPER_FILES	:= $(wildcard Makefile* *.mak) init.lsp

DIRS	:= $(sort $(dir $(HH_FILES)))
CCFLAGS	:= -W -Wall -std=gnu++14 $(DIRS:%=-I %)

TARGET	:= lisp

###########################################################

default: all

all: $(TARGET)

$(TARGET): $(O_FILES) $(O_READER_FILES) $(HELPER_FILES) 
	$(CC) $(O_FILES) $(O_READER_FILES) -o $@

$(O_FILES): %.o: %.cc $(HH_FILES) $(HELPER_FILES)
	$(CC) $(CCFLAGS) -c $< -o $@

clean:
	-rm -f $(O_FILES) $(TARGET)
	-rm $$(find src -iname '*.*.hh' -o -iname '*.*.cc' -o -iname '*.*.o')
	-rm -r _Doxydoc

mrproper: clean
	-rm $(TARGET)

indent: clean
	-clang-format -verbose -style=Google -i $(CC_FILES) $(HH_FILES) 

run: $(TARGET)
	rlwrap ./$(TARGET)

###########################################################

doc: clean
	doxygen Doxygen/Doxyfile
	$(OPEN) _Doxydoc/html/index.html

###########################################################

# https://git-scm.com/docs/git-archive

# The zip rule generates the zip archive, extracts it in /tmp, 
# and then tests the interpret

GROUP := Nom1Nom2Nom3Nom4
GROUP := Bouge
VERSION := HEAD

zip: mrproper all mrproper indent
	git archive --output="$(GROUP).zip" --format=zip --prefix="$(GROUP)/" \
		$(VERSION) src $(HELPER_FILES)
	cp $(GROUP).zip /tmp/$(GROUP).zip; cd /tmp/; unzip -o $(GROUP).zip; \
				cd $(GROUP); cat init.lsp | make run

###########################################################

$(O_READER_FILES): $(SOURCE_READER_FILES) $(HELPER_FILES) 
	# Bison generates $(HH_BISON_FILES)
	bison --defines=$(HH_BISON_FILES) -o $(CC_BISON_FILES) $(SOURCE_BISON_FILES)
	# Flex includes $(HH_BISON_FILES)
	flex -o $(CC_FLEX_FILES) $(SOURCE_FLEX_FILES)
	g++ $(CCFLAGS) -Wno-unused-function -c -o $(O_FLEX_FILES) $(CC_FLEX_FILES) 
	g++ $(CCFLAGS) -Wno-unused-function -c -o $(O_BISON_FILES) $(CC_BISON_FILES) 
