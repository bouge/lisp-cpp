
PLATFORM := Unknown

SYSTEM := $(shell uname -s)
ifeq ($(SYSTEM), Darwin)
	PLATFORM := Darwin
else
	PLATFORM := Linux
endif

ifeq ($(PLATFORM), Unknown)
$(error Unknown platform: Please check Makefile configuration)
endif

$(info Platform: $(PLATFORM))

###########################################################

ifeq ($(PLATFORM), Linux)
# Ubuntu
CC := g++
OPEN := xdg-open
endif

###########################################################

ifeq ($(PLATFORM), Darwin)
# MacOS 10.14
CC := g++
OPEN := open
endif

###########################################################