#!/ bin / sh

OPTIONS='
--language=PostScript --missing-characters
--borders --nup=2 --word-wrap --mark-wrapped=arrow
'

LIST_DIRS=.

for DIR in ${LIST_DIRS}
do
if [ ! -d "$DIR" ]; then continue; fi

echo "$DIR"
REGEX='^(.*\.cc|.*\.hh|.*\.sh|Make.*|README.*)$'
find -E "$DIR" -iregex $REGEX ! -name '*.*.*' -print0 | xargs -0 -I% echo % > a.txt

(set -x 
enscript -o "$DIR"/a.ps ${OPTIONS} $(< a.txt) 
pstopdf "$DIR"/a.ps -o "$DIR"/listing.pdf 
rm a.ps a.txt)

done
