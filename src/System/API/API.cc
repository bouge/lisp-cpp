#include <cassert>
#include <string>

#include "API.hh"
#include "cell.hh"

using namespace std;

// Internal helper function for type promotion

Cell_number* API::object_to_cell_number_pointer(Object l) {
  assert(API::numberp(l));
  Cell_number* obj = dynamic_cast<Cell_number*>(l);
  assert(obj != nullptr);
  return obj;
}

Cell_string* API::object_to_cell_string_pointer(Object l) {
  assert(API::stringp(l));
  Cell_string* obj = dynamic_cast<Cell_string*>(l);
  assert(obj != nullptr);
  return obj;
}

Cell_symbol* API::object_to_cell_symbol_pointer(Object l) {
  assert(API::symbolp(l));
  Cell_symbol* obj = dynamic_cast<Cell_symbol*>(l);
  assert(obj != nullptr);
  return obj;
}

Cell_pair* API::object_to_cell_pair_pointer(Object l) {
  assert(API::pairp(l));
  Cell_pair* obj = dynamic_cast<Cell_pair*>(l);
  assert(obj != nullptr);
  return obj;
}

// Regular API members

const Object API::object_nil = API::symbol_to_object("nil");
const Object API::object_t = API::symbol_to_object("#t");
const Object API::object_f = API::symbol_to_object("#f");

void API::check(Object l) {
  assert(l != nullptr);
  l->check();
}

Object API::cons(Object a, Object l) {
  check(a);
  check(l);
  Object obj = new Cell_pair(a, l);
  return obj;
}

Object API::car(Object l) {
  check(l);
  assert(pairp(l));
  Cell_pair* obj = object_to_cell_pair_pointer(l);
  return obj->get_car();
}

Object API::cdr(Object l) {
  check(l);
  assert(pairp(l));
  Cell_pair* obj = object_to_cell_pair_pointer(l);
  return obj->get_cdr();
}

bool API::numberp(Object l) {
  check(l);
  return l->get_sort() == Cell::sort::NUMBER;
}

bool API::stringp(Object l) {
  check(l);
  return l->get_sort() == Cell::sort::STRING;
}

bool API::symbolp(Object l) {
  check(l);
  return l->get_sort() == Cell::sort::SYMBOL;
}

bool API::pairp(Object l) {
  check(l);
  return l->get_sort() == Cell::sort::PAIR;
}

Object API::number_to_object(int n) {
  Object obj = new Cell_number(n);
  return obj;
}

Object API::string_to_object(string s) {
  Object obj = new Cell_string(s);
  return obj;
}

Object API::symbol_to_object(string s) {
  Object obj = new Cell_symbol(s);
  return obj;
}

int API::object_to_number(Object l) {
  check(l);
  assert(numberp(l));
  Cell_number* obj = object_to_cell_number_pointer(l);
  return obj->get_contents();
}

string API::object_to_string(Object l) {
  check(l);
  assert(stringp(l) || symbolp(l));
  if (stringp(l)) {
    Cell_string* obj = object_to_cell_string_pointer(l);
    return obj->get_contents();
  }
  if (symbolp(l)) {
    Cell_symbol* obj = object_to_cell_symbol_pointer(l);
    return obj->get_contents();
  }
  assert(false);
}
