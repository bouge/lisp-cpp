#pragma once

#include <string>
#include "globals.hh"

class Cell_number;
class Cell_string;
class Cell_symbol;
class Cell_pair;

class API {
 public:
  static const Object object_nil;
  static const Object object_t;
  static const Object object_f;

  static void check(Object l);

  static Object cons(Object a, Object l);
  static Object car(Object l);
  static Object cdr(Object l);

  static bool numberp(Object l);
  static bool stringp(Object l);
  static bool symbolp(Object l);
  static bool pairp(Object l);

  static Object number_to_object(int n);
  static Object string_to_object(std::string s);
  static Object symbol_to_object(std::string s);

  static int object_to_number(Object l);
  static std::string object_to_string(Object l);

 private:
  // Internal helper function for type promotion
  static Cell_number* object_to_cell_number_pointer(Object l);
  static Cell_string* object_to_cell_string_pointer(Object l);
  static Cell_symbol* object_to_cell_symbol_pointer(Object l);
  static Cell_pair* object_to_cell_pair_pointer(Object l);
};
