#include <cassert>

#include "cell.hh"

using namespace std;

void Cell::trace(string s) { s = s; /* clog << s; */ }  // To avoid the warning

const uint64_t Cell::magic_number = 0xDEADBEEF;

Cell::Cell() { magic = magic_number; }

void Cell::clean() {
  check();
  magic = 0;
}

void Cell::check() const { assert(magic == magic_number); }

Cell::~Cell() {
  trace("x");
  check();
  clean();
}

/*****************************/

Cell_number::Cell_number(int n) : contents(n) { trace("n"); }

Cell::sort Cell_number::get_sort() const { return sort::NUMBER; }

int Cell_number::get_contents() const { return contents; }

/*****************************/

Cell_string::Cell_string(std::string s) : contents(s) { trace("t"); }

Cell::sort Cell_string::get_sort() const { return sort::STRING; }

string Cell_string::get_contents() const { return contents; }

/*****************************/

Cell_symbol::Cell_symbol(std::string s) : contents(s) { trace("s"); }

Cell::sort Cell_symbol::get_sort() const { return sort::SYMBOL; }

string Cell_symbol::get_contents() const { return contents; }

/*****************************/

Cell_pair::Cell_pair(Cell *_car, Cell *_cdr) : car(_car), cdr(_cdr) {
  car->check();
  cdr->check();
  trace("p");
}

Cell::sort Cell_pair::get_sort() const { return sort::PAIR; }

Cell *Cell_pair::get_car() const { return car; }

Cell *Cell_pair::get_cdr() const { return cdr; }
