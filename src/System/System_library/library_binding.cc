#include "library_binding.hh"
#include <cassert>
#include "API.hh"
#include "library_object.hh"

bool bindingp(Binding b) {
  if (!(pairp(b))) return false;
  if (!(stringp(car(b)))) return false;
  if (!(pairp(cdr(b)))) return false;
  if (!(null(cdr(cdr(b))))) return false;
  return true;
}

Binding make_binding(std::string name, Object value) {
  Binding b = list2(string_to_object(name), value);
  assert(bindingp(b));
  return b;
}

std::string get_name_binding(Binding b) {
  assert(bindingp(b));
  Object name = car(b);
  return object_to_string(name);
}

Object get_value_binding(Binding b) {
  assert(bindingp(b));
  return cadr(b);
}
