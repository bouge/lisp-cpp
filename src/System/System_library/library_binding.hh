#pragma once

#include <string>
#include "library_object.hh"

using Binding = Object;

void error_no_binding(std::string name);
bool bindingp(Binding b);
Binding make_binding(std::string name, Object value);
std::string get_name_binding(Binding b);
Object get_value_binding(Binding b);