#include "library_env.hh"
#include <cassert>
#include "library_object.hh"

Env make_env() { return nil(); }

bool is_empty_env(Env env) { return null(env); }

Binding get_first_binding_env(Env env) {
  assert(!is_empty_env(env));
  Binding b = car(env);
  assert(bindingp(b));
  return b;
}

Env get_rest_env(Env env) {
  assert(!is_empty_env(env));
  return cdr(env);
}

std::ostream& print_env(std::ostream& s, Env env) {
  s << "\t| ";
  Object alist = env;
  for (Object l = alist; !null(l); l = cdr(l)) {
    assert(!is_empty_env(l));
    Binding b = get_first_binding_env(env);
    std::string name = get_name_binding(b);
    Object value = get_value_binding(b);
    s << name << ": " << value << "; ";
  }
  return s;
}

Env add_new_binding(std::string name, Object value, Env env) {
  Object alist = env;
  Object b = make_binding(name, value);
  return cons(b, alist);
}

Object find_value(std::string name, Env env) {
  for (Env e = env; !is_empty_env(e); e = get_rest_env(e)) {
    assert(!is_empty_env(e));
    Binding b = get_first_binding_env(e);
    std::string s = get_name_binding(b);
    if (s == name) return get_value_binding(b);
  }
  error_no_binding(name);
  return nil();  // To avoid warning "control reaches end of non-void function"
}

/*********************************/

No_binding_exception::No_binding_exception(std::string _name)
    : runtime_error("No binding for name: " + _name) {
  name = _name;
}

void error_no_binding(std::string name) { throw No_binding_exception(name); }
