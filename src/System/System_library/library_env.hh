#pragma once

#include <stdexcept>
#include <string>
#include "API.hh"
#include "globals.hh"
#include "library_binding.hh"

// Exported

Env make_env();
Object find_value(std::string name, Env env);
std::ostream& print_env(std::ostream& s, Env env);
Env add_new_binding(std::string name, Object value, Env env);

class No_binding_exception : public std::runtime_error {
 private:
  std::string name;

 public:
  No_binding_exception(std::string _name);
};

// Internal helpers

bool is_empty_env(Env env);
Binding get_first_binding_env(Env env);
Env get_rest_env(Env env);
void error_no_binding(std::string name);
