#include "library_object.hh"
#include <cassert>
#include "API.hh"

// Derived function

Object cadr(Object l) { return car(cdr(l)); }
Object cddr(Object l) { return cdr(cdr(l)); }
Object caddr(Object l) { return car(cddr(l)); }
Object cdddr(Object l) { return cdr(cddr(l)); }
Object cadddr(Object l) { return car(cdddr(l)); }

Object list2(Object l1, Object l2) { return (cons(l1, (cons(l2, nil())))); }

// Object

void check(Object l) { return API::check(l); }

Object nil() { return API::object_nil; };
Object t() { return API::object_t; };
Object f() { return API::object_f; };

bool null(Object l) {
  check(l);
  return l == nil();
}
Object cons(Object a, Object l) { return API::cons(a, l); };
Object car(Object l) { return API::car(l); };
Object cdr(Object l) { return API::cdr(l); };

bool eq(Object a, Object b) {
  check(a);
  check(b);
  if (null(a) && null(b)) return true;
  if (numberp(a) && numberp(b))
    return (object_to_number(a) == object_to_number(b));
  if ((stringp(a) && (stringp(b))) || (symbolp(a) && symbolp(b)))
    return object_to_string(a) == object_to_string(b);
  return a == b;
}

bool numberp(Object l) { return API::numberp(l); };
bool stringp(Object l) { return API::stringp(l); };
bool symbolp(Object l) { return API::symbolp(l); };
bool pairp(Object l) { return API::pairp(l); };
bool listp(Object l) { return null(l) || pairp(l); }

Object number_to_object(int n) { return API::number_to_object(n); };
Object string_to_object(std::string s) { return API::string_to_object(s); };
Object symbol_to_object(std::string s) { return API::symbol_to_object(s); };

Object bool_to_object(bool b) {
  if (b) return t();
  return f();
}

int object_to_number(Object l) { return API::object_to_number(l); };
std::string object_to_string(Object l) { return API::object_to_string(l); };

bool object_to_bool(Object l) {
  check(l);
  if (null(l)) return false;
  if (symbolp(l)) {
    std::string s = object_to_string(l);
    if (s == "#t") return true;
    if (s == "#f") return false;
  }
  return true;
}

std::ostream& operator<<(std::ostream& s, Object l) {
  return print_object(s, l);
}

void print_object_aux(std::ostream& s, Object l) {
  API::check(l);
  assert(listp(l));
  if (null(l)) return;
  assert(pairp(l));
  Object obj1 = car(l);
  Object obj2 = cdr(l);
  print_object(s, obj1);

  if (null(obj2)) {
    return;
  }

  if (!pairp(obj2)) {
    s << " . ";
    print_object(s, obj2);
    return;
  }

  s << " ";
  print_object_aux(s, obj2);
  return;
}

std::ostream& print_object(std::ostream& s, Object l) {
  API::check(l);
  if (null(l)) return s << "()";
  if (numberp(l)) return s << object_to_number(l);
  if (stringp(l)) return s << object_to_string(l);
  if (symbolp(l)) return s << object_to_string(l);
  assert(pairp(l));
  s << "(";
  print_object_aux(s, l);
  s << ")";
  return s;
}