#pragma once

#include <iostream>
#include <stdexcept>
#include <string>

#include "API.hh"
#include "globals.hh"

void check(Object l);

Object nil();
Object t();
Object f();

bool null(Object l);
Object cons(Object a, Object l);
Object car(Object l);
Object cdr(Object l);
bool eq(Object a, Object b);

bool numberp(Object l);
bool stringp(Object l);
bool symbolp(Object l);
bool pairp(Object l);
bool listp(Object l);

Object number_to_object(int n);
Object string_to_object(std::string s);
Object symbol_to_object(std::string s);
Object bool_to_object(bool b);

int object_to_number(Object l);
std::string object_to_string(Object l);
bool object_to_bool(Object l);

std::ostream& print_object(std::ostream& s, Object l);
std::ostream& operator<<(std::ostream& s, Object l);

// Derived function

Object cadr(Object l);
Object cddr(Object l);
Object caddr(Object l);
Object cdddr(Object l);
Object cadddr(Object l);

Object list2(Object l1, Object l2);
