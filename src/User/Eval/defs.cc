#include "defs.hh"
#include <cassert>
#include "eval.hh"

using namespace std;

void check_name(Object l, std::string s) {
  assert(object_to_string(car(l)) == s);
}

Object do_lambda(Object l) {
  check_name(l, lisp_lambda);
  return l;
}

Object do_quote(Object l) {
  check_name(l, lisp_quote);
  return cadr(l);
}

Object do_if(Object l, Env env) {
  check_name(l, lisp_if);
  Object test_part = cadr(l);
  Object then_part = caddr(l);
  Object else_part = cadddr(l);
  Object test_value = eval(test_part, env);
  if (object_to_bool(test_value)) return eval(then_part, env);
  return eval(else_part, env);
}

Object do_printenv(Object l, Env env) {
  check_name(l, lisp_printenv);
  print_env(cout, env);
  return nil();
}

Object do_plus(Object lvals) {
  int a = object_to_number(car(lvals));
  int b = object_to_number(cadr(lvals));
  return number_to_object(a + b);
}

Object do_minus(Object lvals) {
  int a = object_to_number(car(lvals));
  int b = object_to_number(cadr(lvals));
  return number_to_object(a - b);
}

Object do_times(Object lvals) {
  int a = object_to_number(car(lvals));
  int b = object_to_number(cadr(lvals));
  return number_to_object(a * b);
}

Object do_eq(Object lvals) {
  Object a = car(lvals);
  Object b = cadr(lvals);
  return bool_to_object(eq(a, b));
}

Object do_car(Object lvals) {
  Object a = car(lvals);
  return car(a);
}

Object do_cdr(Object lvals) {
  Object a = car(lvals);
  return cdr(a);
}

Object do_cons(Object lvals) {
  Object a = car(lvals);
  Object b = cadr(lvals);
  return cons(a, b);
}

Object do_null(Object lvals) {
  Object a = car(lvals);
  return bool_to_object(null(a));
}
