#include "eval.hh"
#include <cassert>
#include "defs.hh"

using namespace std;

Object eval_list(Object largs, Env env);
Env extend_env(Object lpars, Object lvals, Env env);
void error_evaluation(Object obj, Env env, string message);
void error_zipping(Object lobjs, string message);

Object eval(Object l, Env env) {
  // clog << "\teval: " << l << "\t|\t" << env << endl;

  if (null(l)) return l;
  if (eq(l, t())) return l;  // Watch it! Using eq is mandatory here
  if (eq(l, f())) return l;  // Watch it! Using eq is mandatory here
  if (numberp(l)) return l;
  if (stringp(l)) return l;
  if (symbolp(l)) {
    string s = object_to_string(l);
    return find_value(s, env);
  }
  assert(pairp(l));
  Object f = car(l);
  if (symbolp(f)) {
    string s = object_to_string(f);
    if (s == lisp_lambda) return do_lambda(l);
    if (s == lisp_quote) return do_quote(l);
    if (s == lisp_if) return do_if(l, env);
    if (s == lisp_printenv) return do_printenv(l, env);
  }
  // Apply f to the list of argument values
  Object vals = eval_list(cdr(l), env);
  return apply(f, vals, env);
}

Object apply(Object f, Object lvals, Env env) {
  // clog << "\tapply: " << f << " " << lvals << endl;

  if (null(f)) error_evaluation(f, env, "Cannot apply nil");
  if (numberp(f)) error_evaluation(f, env, "Cannot apply a number");
  if (stringp(f)) error_evaluation(f, env, "Cannot apply a string");
  if (symbolp(f)) {
    string s = object_to_string(f);
    // f is a known subr
    if (s == lisp_plus) return do_plus(lvals);
    if (s == lisp_minus) return do_minus(lvals);
    if (s == lisp_times) return do_times(lvals);
    if (s == lisp_eq) return do_eq(lvals);
    if (s == lisp_car) return do_car(lvals);
    if (s == lisp_cdr) return do_cdr(lvals);
    if (s == lisp_cons) return do_cons(lvals);
    if (s == lisp_null) return do_null(lvals);
    // f is any other symbol
    Object new_f = eval(f, env);
    return apply(new_f, lvals, env);
  }
  // f is a pair
  assert(pairp(f));
  Object ff = car(f);
  if (symbolp(ff)) {
    string s = object_to_string(ff);
    // ff is lambda
    if (s == lisp_lambda) {
      Object lpars = cadr(f);
      Object body = caddr(f);
      Env new_env = extend_env(lpars, lvals, env);
      return eval(body, new_env);
    }
  }
  // ff is anything else
  error_evaluation(f, env, "Cannot apply a list");
  assert(false);
}

/****************************************************/

Object eval_list(Object largs, Env env) {
  if (null(largs)) return largs;
  return cons(eval(car(largs), env), eval_list(cdr(largs), env));
}

Env extend_env(Object lpars, Object lvals, Env env) {
  if (null(lpars) && null(lvals)) return env;
  if (null(lpars) && !null(lvals)) error_zipping(lvals, "Too many values");
  if (!null(lpars) && null(lvals)) error_zipping(lpars, "Too many parameters");
  // clog << env << std::endl;
  string par = object_to_string(car(lpars));
  Object val = car(lvals);
  Env new_env = add_new_binding(par, val, env);
  return extend_env(cdr(lpars), cdr(lvals), new_env);
}

/****************************************************/

Evaluation_exception::Evaluation_exception(Object _obj, Env _env,
                                           string _message)
    : runtime_error("Evaluation error: " + _message) {
  obj = _obj;
  env = _env;
  message = _message;
}

void error_evaluation(Object obj, Env env, string message) {
  throw Evaluation_exception(obj, env, message);
}

Zipping_exception::Zipping_exception(Object _lobjs, string _message)
    : runtime_error("Zipping exception: " + _message) {
  message = _message;
  lobjs = _lobjs;
}

void error_zipping(Object lobjs, string message) {
  throw Zipping_exception(lobjs, message);
}