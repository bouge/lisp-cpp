#include "read.hh"
#include <iostream>
#include <stdexcept>

using namespace std;

extern Object get_read_object();
extern int yyparse();

void error_EOF() { throw runtime_error("End of input stream"); }

Object read_object() {
  if (yyparse() != 0) error_EOF();
  Object l = get_read_object();
  cout << "Read: " << l << endl;
  return l;
}
