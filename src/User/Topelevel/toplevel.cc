#include "toplevel.hh"
#include <iostream>
#include "eval.hh"
#include "library.hh"
#include "read.hh"

using namespace std;

Toplevel::Toplevel() { global_env = make_env(); }

// Watch the side-effect!
bool check_directive(Object l, Env& env);

/*****************************************/

void Toplevel::go(bool use_prompt) {
  while (true) {
    if (use_prompt) cout << "Lisp? " << flush;
    Object obj = read_object();
    try {
      // check_directive may modify env
      if (check_directive(obj, global_env)) continue;
      Object l = eval(obj, global_env);
      cout << l << endl;
    } catch (runtime_error& e) {
      clog << e.what() << endl;
    }
  }
}

/*****************************************/

// Watch the side-effect!
bool check_directive(Object l, Env& env) {
  if (!pairp(l)) return false;
  Object f = car(l);
  if (!symbolp(f)) return false;
  string s = object_to_string(f);
  if (s != "define") return false;

  /* Directive recognized */
  if (!pairp(cdr(l))) return false;
  string name = object_to_string(cadr(l));
  if (!pairp(cddr(l))) return false;
  Object definition = caddr(l);
  Object value = eval(definition, env);
  env = add_new_binding(name, value, env);
  cout << "Define: " << name << " = " << value << endl;
  return true;
}
