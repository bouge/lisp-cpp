#pragma once

#include <iostream>
#include <stdexcept>
#include <string>
#include "globals.hh"

// library_binding.hh

void error_no_binding(std::string name);
bool bindingp(Binding b);
Binding make_binding(std::string name, Object value);
std::string get_name_binding(Binding b);
Object get_value_binding(Binding b);

// library_env.hh

Env make_env();
Object find_value(std::string name, Env env);
std::ostream& print_env(std::ostream& s, Env env);
Env add_new_binding(std::string name, Object value, Env env);

class No_binding_exception : public std::runtime_error {
 private:
  std::string name;

 public:
  No_binding_exception(std::string _name);
};

// library_object.hh

void check(Object l);

Object nil();
Object t();
Object f();

bool null(Object l);
Object cons(Object a, Object l);
Object car(Object l);
Object cdr(Object l);
bool eq(Object a, Object b);

bool numberp(Object l);
bool stringp(Object l);
bool symbolp(Object l);
bool pairp(Object l);
bool listp(Object l);

Object number_to_object(int n);
Object string_to_object(std::string s);
Object symbol_to_object(std::string s);
Object bool_to_object(bool b);

int object_to_number(Object l);
std::string object_to_string(Object l);
bool object_to_bool(Object l);

std::ostream& print_object(std::ostream& s, Object l);
std::ostream& operator<<(std::ostream& s, Object l);

// Derived function

Object cadr(Object l);
Object cddr(Object l);
Object caddr(Object l);
Object cdddr(Object l);
Object cadddr(Object l);

Object list2(Object l1, Object l2);
